//Andrew Hessler-Loch

// CS 350, Fall 2015
// Lab 5
// Rational number structure
//

#include <stdio.h>
#include "Lab5_rational.h"

// Utility: If x, y > 0, return greatest common divisor
// else return 0
//
int gcd(int x, int y) {
	// gcd of current x, y = gcd of original x, y
	// because gcd(x,y) = gcd(x-y,y) [if x > y] or gcd(x, y-x) [if x < y]
	// and gcd(x,x) = x
	//
	while (x != y) {
		if (x < y)
			y = y-x;
		else
			x = x-y;
	}
	return x;
}


void set_rat(Rational *r, int n, int d){
	(*r).numerator = n;
	(*r).denominator = d;
	//normalize(r);
}

void copy_rat(Rational *r, Rational *s){
	(*s).numerator = (*r).numerator;
	(*s).denominator = (*r).denominator;
	//normalize(s);
	//normalize(r);
}

double value(Rational *r){
	double n, d;
	n = (*r).numerator;
	d = (*r).denominator;

	return n/d;
}

void add_rat(Rational *r, Rational *s){
	int newn = (*r).numerator * (*s).denominator + (*s).numerator *(*r).denominator;
	int newd = (*r).denominator * (*s).denominator;

	(*r).numerator = newn;
	(*r).denominator = newd;
	//normalize(r);
	//normalize(s);
}

void sub_rat(Rational *r, Rational *s){
	int newn = (*r).numerator * (*s).denominator - (*s).numerator *(*r).denominator;
	int newd = (*r).denominator * (*s).denominator;

	(*r).numerator = newn;
	(*r).denominator = newd;
	//normalize(r);
	//normalize(s);
}

void mul_rat(Rational *r, Rational *s){
	int newn = (*r).numerator * (*s).numerator;
	int newd = (*r).denominator * (*s).denominator;

	(*r).numerator = newn;
	(*r).denominator = newd;
	//normalize(r);
	//normalize(s);
}

void div_rat(Rational *r, Rational *s){
	int newn = (*r).numerator * (*s).denominator;
	int newd = (*r).denominator * (*s).numerator;

	(*r).numerator = newn;
	(*r).denominator = newd;
	//normalize(r);
	//normalize(s);
}

void print_rat(Rational *r, int p){
	double num, den;
	//normalize(r);
	num = (*r).numerator;
	den = (*r).denominator;
	double tobeprinted = num / den;
	printf("%.*f", p, tobeprinted);
}

void print_ratio(Rational *r){
	normalize(r);
	printf("%d:%d", (*r).numerator, (*r).denominator);
}

void normalize(Rational *r){
	if((*r).numerator == 0 || (*r).denominator == 0){
		(*r).numerator = 0;
		(*r).denominator = 1;
	}
	else if((*r).numerator < 0 && (*r).denominator < 0){
		(*r).numerator = -(*r).numerator;
		(*r).denominator = -(*r).denominator;

		int factor = gcd((*r).numerator, (*r).denominator);
		(*r).numerator = (*r).numerator / factor;
		(*r).denominator = (*r).denominator / factor;
	}
	else if((*r).numerator < 0){
		(*r).numerator = -(*r).numerator;

		int factor = gcd((*r).numerator, (*r).denominator);
		(*r).numerator = (*r).numerator / factor;
		(*r).denominator = (*r).denominator / factor;
		(*r).numerator = -(*r).numerator;
	}
	else if((*r).denominator < 0){
		(*r).denominator = -(*r).denominator;

		int factor = gcd((*r).numerator, (*r).denominator);
		(*r).numerator = (*r).numerator / factor;
		(*r).denominator = (*r).denominator / factor;
		(*r).numerator = -(*r).numerator;		
	}
	else{
		int factor = gcd((*r).numerator, (*r).denominator);
		(*r).numerator = (*r).numerator / factor;
		(*r).denominator = (*r).denominator / factor;
	}
	
}
