//Andrew Hessler-Loch CS330 Lab 4
#include <stdio.h>

int main(int argc, char *argv[])
{
	printf("Welcome to Andrew Hessler-Loch's Lab4\n\n");

	//Read command-line argument.
	printf("The command line had %d part(s)\n", argc);
	int i;

	//List arguments.
	for(i = 0; i < argc; i++)
		printf("argv[%d] = %s\n", i, argv[i]);

	//If wrong number of command line arguments: end.
	if(argc != 2 && argc != 1) {
		printf("\nBad command; should be:\n %s filename\n or %s\n", argv[0]);
		return 1;
	}

	//File access. If no command-line file given, use "default.txt".
	char *filename;
	FILE *in_file;
	if(argc == 1){
		char* hold = "default.txt";
		filename = hold;
	}
	else{
		char *filename = argv[1];
	}

	printf("Opening file: %s\n", filename);
	
	in_file = fopen(filename, "r");	

	//If file didn't open, return null.'
	if(in_file == NULL){
		printf("Couldn't open file %s\n", filename);
		return 1;
	}

	int nbr_values_read;
	unsigned int value_read_X, value_read_L, value_read_R;
	unsigned int mask = 0;
	
	//Initial scan before entering the loop.
	nbr_values_read = fscanf(in_file, "%x%d%d", &value_read_X, &value_read_L, &value_read_R);
	while(nbr_values_read > 0){
		printf("Read\nX = 0x%x\nL = %d\nR = %d\n", value_read_X, value_read_L, value_read_R);
		printf("value          0x%x = %d\n", value_read_X, value_read_X);

		//Loop must be used because alternative method used beforehand may force the bits to exceed 32 bits.
	  for(int i = value_read_L; i <= value_read_R; i++){
			mask |= 1 << i;
		}
		
		printf("mask           0x%x, bits %d:%d\n", mask, value_read_L, value_read_R);
		printf("selected bits  0x%x, right-aligned 0x%x\n", mask & value_read_X, ((mask & value_read_X) >> value_read_L));

		//+1's may be temporary, but currently the only way to get appropriate output, maybe a carry problem?
		printf("bits set       0x%x\n", (mask | value_read_X));
		printf("bits cleared   0x%x\n", (~mask & value_read_X));
		printf("bits flipped   0x%x\n", (mask ^ value_read_X));

		//Thinking Area
		
		//bits flipped
		//1010 1011 1100 1101 1110 1111 1010 1011
		//                    0111 1111 1100 0000
		//1010 1011 1100 1101 1001 0000 0110 1011

		//bits cleared
		//1010 1011 1100 1101 1110 1111 1010 1011
		//            neg     0111 1111 1100 0000
		//1010 1011 1100 1101 1000 0000 0010 1011

		//bits set
		//1010 1011 1100 1101 1110 1111 1010 1011
		//                    0111 1111 1100 0000
		//1010 1011 1100 1101 1111 1111 1110 1011
		mask = 0;
		nbr_values_read = fscanf(in_file, "%x%d%d", &value_read_X, &value_read_L, &value_read_R);
	}

	//Try to close file.
	int close_err = fclose(in_file);
	if(close_err){
		printf("File close failed!\n");
	}
	
}
